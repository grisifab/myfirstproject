package exo2;

import java.util.Scanner;

public class Exo2 {

	public static void main(String[] args) {
		// �crivez un programme Java qui demande deux chiffres � un utilisateur, 
		// les divise puis affiche le r�sultat sur l'�cran. 
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("un nombre :");
		int i = monScanner.nextInt();
		System.out.println("un autre nombre :");
		int j = monScanner.nextInt();
		monScanner.close();
		float res = (float)i / (float)j;
		System.out.println(i + " / " + j + " = " + res);
		
		
	}

}
