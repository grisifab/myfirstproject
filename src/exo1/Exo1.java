package exo1;

import java.util.Scanner;

public class Exo1 {

	public static void main(String[] args) {
		// �crivez un programme Java qui demande son nom � l�utilisateur 
		// et qui affiche �Bonjour� � l'�cran et le nom sur une ligne distincte.
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("ton nom : ?");
		String s1 = monScanner.next();
		//String s2 = monScanner.nextLine(); //pour vider le scanner
		System.out.println("Bonjour");
		System.out.println(s1);
		monScanner.close();
	}

}
