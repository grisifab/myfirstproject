package exo4;

import java.util.Scanner;

public class Exo4 {

	public static void main(String[] args) {
		// �crivez un programme Java qui demande un chiifre (RAYON) � l�utilisateur 
		//et qui affiche l'aire et le p�rim�tre d'un cercle 
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("rayon du cercle");
		float rayon = monScanner.nextFloat();
		monScanner.close();
		double peri = 2 * rayon * Math.PI;
		double surface = Math.PI * rayon * rayon;
		System.out.println("Le perim�tre est de : " + peri);
		System.out.println("La surface est de : " + surface);
		
	}

}
